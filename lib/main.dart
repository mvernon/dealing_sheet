import 'package:flutter/material.dart';
import 'dart:math';

/*
Copyright (C) 2022 Matthew Vernon <mvernon@wikimedia.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/
*/

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dealing Sheet',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Custom colour based on 0x85B09A, which is our icon colour
        primarySwatch: const MaterialColor(
          0xFF668F77,
          <int, Color>{
            50: Color(0xFFE7F1ED),
            100: Color(0xFFC4DCD1),
            200: Color(0xFFA2C7B4),
            300: Color(0xFF85B09A),
            400: Color(0xFF759F88),
            500: Color(0xFF668F77),
            600: Color(0xFF5F826D),
            700: Color(0xFF567360),
            800: Color(0xFF4C6353),
            900: Color(0xFF3A473D),
          },
        ),
      ),
      home: const MyHomePage(title: 'Bridge Dealing Sheet'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<int> _cards = [for (var i = 0; i < 52; i++) i];
  int _cols = 4;
  final int _perbox = 4;

  //Shuffle the cards once when creating this State object
  @override
  void initState() {
    super.initState();
    _cards.shuffle();
  }

  void _shuffleDeck() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _cards.shuffle();
    });
  }

  DataRow makerow(List<int> cards, [int cols = 4, int perbox = 4]) {
    assert(cards.isNotEmpty);
    var cells = <DataCell>[];
    for (var i = 0; i < cols; i++) {
      int start = i * perbox;
      if (start - 1 > cards.length) {
        cells.add(const DataCell(Text('')));
        continue;
      }
      int end =
          ((i + 1) * perbox >= cards.length) ? cards.length : (i + 1) * perbox;
      cells.add(DataCell(
          Text(cards.getRange(start, end).map((i) => i.toString()).join(''))));
    }
    return DataRow(cells: cells);
  }

  List<DataRow> makerows(List<int> cards, [int cols = 4, int perbox = 4]) {
    List<int> hands = [for (var i = 0; i < 52; i++) (cards.indexOf(i) % 4) + 1];
    var ret = <DataRow>[];
    final perrow = cols * perbox;
    for (var i = 0; i < hands.length / perrow; i++) {
      int start = i * perrow;
      int end = ((i + 1) * perrow >= hands.length)
          ? hands.length
          : ((i + 1) * perrow);
      ret.add(makerow(
          hands.getRange(start, end).toList(), cols = cols, perbox = perbox));
    }
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _shuffleDeck method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    MediaQueryData media = MediaQuery.of(context);
    assert(_cards.isNotEmpty);
    if (_cards.isNotEmpty && media.size.width > 0) {
      _cols = min(media.size.width ~/ 125, (_cards.length / _perbox) ~/ 2);
    }
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: DataTable(
        headingRowHeight: 0,
        dataTextStyle: Theme.of(context).textTheme.headlineMedium,
        columns: <DataColumn>[
          for (var i = 0; i < _cols; i++)
            const DataColumn(
              label: Text(''),
              numeric: true,
            )
        ],
        border: TableBorder.all(),
        rows: makerows(_cards, _cols, _perbox),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _shuffleDeck,
        tooltip: 'Shuffle',
        child: const Icon(Icons.shuffle_rounded),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
