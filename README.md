# Dealing Sheet
![F-Droid version](https://img.shields.io/f-droid/v/org.principate.matthew.dealing_sheet.svg)
![Latest Tag](https://img.shields.io/gitlab/v/tag/mvernon/dealing_sheet?gitlab_url=https%3A%2F%2Fgitlab.wikimedia.org&include_prereleases)

Dealing Sheet is an app that shows you a dealing sheet, letting you
properly randomise a deck of 52 cards into 4 hands without having to
shuffle them yourself. This is useful for games like bridge.

The app is responsive, adjusting the number of columns to fit the
width of your device, though it will always build at least two rows.

## Using a dealing sheet

Take your deck of cards (if you're worried that someone might have
memorized the location of key cards, cut the deck a couple of times),
and deal the cards into four piles, putting the cards onto the piles
indicated in the boxes. For example, if the first box in the sheet
says "4331", deal the top card into the fourth (right-most) pile, the
next two into the third pile, and the next into the first (left-most)
pile. Continue thus until you have dealt out all the cards (which
should also be the end of the dealing sheet), pass one pile to each
player (conventionally first pile to left-hand opponent, second to
player opposite, third to right-hand opponent, fourth for dealer), and
check everyone has thirteen cards.

## How does it work?

Internally, we have an array of members, representing the cards in the
deck. This deck is shuffled, and then the position of each card is
inspected to work out which player's hand it ends up in and thus which
pile it should be added to - e.g. if the card that started off in
position 6 is now in position 1, we know that the 6th card must be in
pile 1. 

## Other games

This approach works well for games where you deal out most of the deck
and the order within hands isn't important. It would work quite well
for a game like cribbage where you have essentially 4 unequal piles
(two hands of 6, one tunerover, and the rest of the deck) too. It
won't work very well for games where you need to shuffle the entire
deck into one pile (you could make this work by shuffling the deck
into 52 piles and then combining them again, but that's quite hard!).

It should be fairly straightforward to extend this application to deal
other patterns - the DataTable is constructed procedurally based on
the number of columns and cards.

## Platform Support

This app is Free Software (see [LICENSE](LICENSE) for details),
written using the [Flutter](https://flutter.dev/) framework. It should
be possible to build for Android, Linux, Web, macOS, iOS, Google
Fuschia, and Windows from this codebase, although the author has only
tested the first three platforms.

See the [upstream docs](https://docs.flutter.dev/) for instructions on
building for other platforms; if there are changes needed to the code
to make it work better elsewhere, please let me know!

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="75">](https://f-droid.org/packages/org.principate.matthew.dealing_sheet)
